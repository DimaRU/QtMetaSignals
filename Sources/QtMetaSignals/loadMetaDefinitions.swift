//
//  loadMetaDefinitions.swift
//  
//
//  Created by Dmitriy Borovikov on 25.06.2023.
//

import Foundation

extension QtMetaSignals {
    static func loadMetaDefinitions(from file: URL) -> [QtMeta] {
        do {
            let data = try Data(contentsOf: file)
            let decoder = JSONDecoder()
            let meta = try decoder.decode([QtMeta].self, from: data)
            return meta
        } catch {
            print("Json meta deinitions load error:", error)
            exit(1)
        }
    }
}

import Foundation

@main
struct QtMetaSignals {
    static func main() {

        if CommandLine.arguments.count < 2 {
            print("Usage: QtMetaSignals <directory> <opts>")
            exit(0)
        }
        
        let manager = FileManager.default
        let jsonFiles = try! manager.contentsOfDirectory(at: URL(fileURLWithPath: CommandLine.arguments[1]), includingPropertiesForKeys: nil)
            .filter{ $0.pathExtension == "json" }
        
        if getBoolArg("--classes") {
            printClasses(for: jsonFiles)
        } else {
            printMetatypes(for: jsonFiles)
        }
    }

    static func printClasses(for jsonFiles: [URL]) {
        for file in jsonFiles {
            let moduleName = file.lastPathComponent.split(separator: "_").first!
            let metaDefinitions = loadMetaDefinitions(from: file)
            guard !metaDefinitions.isEmpty else { continue }
            print(moduleName, ":", separator: "", terminator: "\n\n")
            let classes = metaDefinitions.reduce(into: [String](), { partialResult, meta in
                partialResult.append(contentsOf: meta.classes.map{ $0.className} )
            }).sorted()
            classes.forEach{ print($0) }
            print("")
        }
    }

    static func printMetatypes(for jsonFiles: [URL]) {
        var signalArgTypes: Set<String> = []
        var signalArgumentDefs: Set<String> = []
        var signalReturns: Set<String> = []

        var slotArgTypes: Set<String> = []
        var slotArgumentDefs: Set<String> = []
        var slotReturns: Set<String> = []

        for file in jsonFiles {
            var (argTypes, argumentDefs, returns) = parceMetaSignals(file: file)
            signalArgTypes.formUnion(argTypes)
            signalArgumentDefs.formUnion(argumentDefs)
            signalReturns.formUnion(returns)
            
            (argTypes, argumentDefs, returns) = parceMetaSlots(file: file)
            slotArgTypes.formUnion(argTypes)
            slotArgumentDefs.formUnion(argumentDefs)
            slotReturns.formUnion(returns)
        }
        
        print("* Signal argument types *")
        signalArgTypes.sorted(by: <).forEach{ print($0) }
        print("\n* Arguments *")
        signalArgumentDefs.sorted(by: <).forEach{ print($0) }
        print("\n* Signal return types *")
        signalReturns.sorted(by: <).forEach{ print($0) }
        print("\n\n")
        print("* Slot argument types *")
        slotArgTypes.sorted(by: <).forEach{ print($0) }
        print("\n* Arguments *")
        slotArgumentDefs.sorted(by: <).forEach{ print($0) }
        print("\n* Slot return types *")
        slotReturns.sorted(by: <).forEach{ print($0) }
    }
    
    static func parceMetaSignals(file: URL) -> (argTypes: Set<String>,
                                     argumentDefs: Set<String>,
                                     returns: Set<String>) {
        var argTypes: Set<String> = []
        var argumentDefs: Set<String> = []
        var returns: Set<String> = []
        
        let metaDefinitions = loadMetaDefinitions(from: file)
        
        for meta in metaDefinitions {
            for classMeta in meta.classes {
                for signal in classMeta.signals ?? [] {
                    returns.insert(signal.returnType)
                    guard let arguments = signal.arguments else { continue }
                    let types = arguments.map{ $0.type }
                    argTypes.formUnion(types)
                    let args = signal.name + "(" + arguments.map{ ($0.name ?? "-") + ": " + $0.type }.joined(separator: ", ") + ")"
                    argumentDefs.insert(args)
                }
            }
        }
        return (argTypes, argumentDefs, returns)
    }
    
    static func parceMetaSlots(file: URL) -> (argTypes: Set<String>,
                                     argumentDefs: Set<String>,
                                     returns: Set<String>) {
        var argTypes: Set<String> = []
        var argumentDefs: Set<String> = []
        var returns: Set<String> = []
        
        let metaDefinitions = loadMetaDefinitions(from: file)
        
        for meta in metaDefinitions {
            for classMeta in meta.classes {
                for slot in classMeta.slots ?? [] {
                    returns.insert(slot.returnType)
                    guard let arguments = slot.arguments else { continue }
                    let types = arguments.map{ $0.type }
                    argTypes.formUnion(types)
                    let args = slot.name + "(" + arguments.map{ ($0.name ?? "-") + ": " + $0.type }.joined(separator: ", ") + ")"
                    argumentDefs.insert(args)
                }
            }
        }
        return (argTypes, argumentDefs, returns)
    }
}

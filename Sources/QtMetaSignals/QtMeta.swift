//
//  QtMeta.swift
//  
//
//  Created by Dmitriy Borovikov on 25.06.2023.
//

import Foundation

// MARK: - QtMetaElement
struct QtMeta: Decodable {
    let classes: [ClassMeta]
    let inputFile: String
    let outputRevision: Int
}

// MARK: - Class
struct ClassMeta: Decodable {
    let className: String
    let qualifiedClassName: String
    let superClasses: [SuperClass]?
    let properties: [Property]?
    let signals: [Function]?
    let slots: [Function]?
    let enums: [Enum]?
    let methods: [Method]?
    let classInfos: [ClassInfo]?
    let interfaces: [[Interface]]?
    let object: Bool?
    let gadget: Bool?
    
    // MARK: - ClassInfo
    struct ClassInfo: Decodable {
        let name: String
        let value: String
    }
    
    // MARK: - Enum
    struct Enum: Decodable {
        let isClass: Bool
        let isFlag: Bool
        let name: String
        let values: [String]
        let alias: String?
    }
    
    // MARK: - Interface
    struct Interface: Decodable {
        let className: String
        let id: String
    }
    
    // MARK: - Method
    struct Method: Decodable {
        let access: String
        let arguments: [MethodArgument]?
        let name: String
        let returnType: String
        let isCloned: Bool?
    }
    
    // MARK: - MethodArgument
    struct MethodArgument: Decodable {
        let name: String?
        let type: String
    }
    
    // MARK: - Property
    struct Property: Decodable {
        let name: String
        let type: String
        let read: String?
        let write: String?
        let constant: Bool
        let designable: Bool
        let index: Int
        let scriptable: Bool
        let stored: Bool
        let user: Bool
        let notify: String?
        let reset: String?
        let privateClass: String?
        let propertyRequired: Bool?
        let propertyFinal: Bool?
    }
    
    // MARK: - function
    struct Function: Decodable {
        let access: String
        let name: String
        let returnType: String
        let arguments: [SignalArgument]?
        let isCloned: Bool?
    }
    
    // MARK: - SignalArgument
    struct SignalArgument: Decodable {
        let name: String?
        let type: String
    }
    
    // MARK: - SuperClass
    struct SuperClass: Decodable {
        let access: String
        let name: String
    }
}
